﻿using Unity.Entities;
using System;

namespace HeroCraft
{
    //Компонент для хранения 2D позиции все движущиеся объекты и клетки в лабиринте
    [Serializable]
    public struct Pos : IComponentData
    {
        public int x;
        public int y;
        public int hCost;
        public int gCost;
    }

    [Serializable]
    public struct Parent : IComponentData
    {
        public Pos own;
        public Pos pos;
    }

    [Serializable]
    public struct Step : IComponentData
    {
        public Pos next;
        public Pos cur;
    }
    //"Затопленная" клетка для проверки возможности дойти до цели
    [Serializable]
    public struct Flood : IComponentData{   }



    //Компонент при наличии его у клетки клетка обозначена как непроходимая
    [Serializable]
    public struct IsObstacle : IComponentData { }

    //Компонент при наличии его у клетки клетка обозначена как непроходимая
    [Serializable]
    public struct IsPlayer : IComponentData { }


    //Компонент при наличии его у клетки клетка обозначена как начальная точка лабиринта
    [Serializable]
    public struct IsSpawnPoint : IComponentData { }

    //Компонент при наличии его у клетки клетка обозначена как конечная точка выход из лабиринта
    [Serializable]
    public struct IsGoalPoint : IComponentData { }

    //Компонент для всех тайлов который указывает алгоритму генерации лабиринта : сколько соседей у этой клетки - не препятствия
    // возможные значения 2 или 3
    [Serializable]
    public struct Prob : IComponentData
    {
        public int Value;
    }
}

