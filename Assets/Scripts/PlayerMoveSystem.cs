﻿using Unity.Entities;
using UnityEngine;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Transforms;
using Unity.Collections;

namespace HeroCraft
{
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    public class PlayerMoveSystem : JobComponentSystem
    {
        private EntityQuery m_Group_Player;
        private EntityQuery m_Group_Cells;
        private EntityQuery m_Group_Path;

        private float PlayerSpeed = 1f;
        public const float MOVE_PRECISION = 0.05f;

        protected override void OnCreate()
        {
            m_Group_Player = GetEntityQuery(typeof(IsPlayer), typeof(Translation), typeof(Pos));

            var queryCells = new EntityQueryDesc
            {
                None = new ComponentType[] { typeof(IsPlayer), typeof(IsObstacle) },
                All = new ComponentType[] { typeof(Translation), typeof(Pos) },
            };

            var queryPath = new EntityQueryDesc
            {
                All = new ComponentType[] { typeof(Step) },
            };

            m_Group_Cells = GetEntityQuery(queryCells);
            m_Group_Path = GetEntityQuery(queryPath);
        }

        struct StepJob : IJob
        {
            public NativeArray<Pos> playersPos;
            public NativeArray<Translation> playersTrls;

            public NativeArray<Pos> cellsPos;
            public NativeArray<Translation> cellsTrls;
            public NativeArray<Step> cellsStps;

            public NativeArray<int> actualStepX;
            public NativeArray<int> actualStepY;

            public float Time;
            public void Execute()
            {
                var playerPos = playersPos[0];
                var playerTrl = playersTrls[0];
                for (int i = 0; i < cellsPos.Length; i++)
                {
                    if (cellsPos[i].x == playerPos.x && cellsPos[i].y == playerPos.y)
                    {
                        var cellTrlF2 = new float2(cellsTrls[i].Value.x, cellsTrls[i].Value.y);
                        var playerTrlF2 = new float2(playerTrl.Value.x, playerTrl.Value.y);
                        if (math.length(cellTrlF2 - playerTrlF2) < MOVE_PRECISION)
                        {
                            for (int j = 0; j < cellsStps.Length; j++)
                            {
                                if (cellsStps[j].next.x == playerPos.x && cellsStps[j].next.y == playerPos.y)
                                {
                                    actualStepX[0] = cellsStps[j].cur.x;
                                    actualStepY[0] = cellsStps[j].cur.y;
                                    break;
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }

        struct MoveSpeedJob : IJobForEachWithEntity<Translation, Pos, IsPlayer>
        {
            public float Speed;
            public float DeltaTime;

            public NativeArray<int> actualStepX;
            public NativeArray<int> actualStepY;

            public void Execute(Entity entity, int index, ref Translation translation, ref Pos pos, [ReadOnly] ref IsPlayer isPlayer)
            {
                if (actualStepX[0] != pos.x || actualStepY[0] != pos.y)
                {
                    int posx = actualStepX[0];
                    int posy = actualStepY[0];

                    pos.x = posx;
                    pos.y = posy;
                }

                var plTarTrl = new float2()
                {
                    x = pos.x * Generator.TILE_MAGN - Generator.Offset.x,
                    y = pos.y * Generator.TILE_MAGN - Generator.Offset.y
                };

                float resX = translation.Value.x, resY = translation.Value.y;
                var delta = DeltaTime * Speed;  

                if (resX < plTarTrl.x)
                {
                    resX += delta;
                    if (resX > plTarTrl.x) resX = plTarTrl.x;
                }
                else if (resX > plTarTrl.x)
                {
                    resX -= delta;
                    if (resX < plTarTrl.x) resX = plTarTrl.x;
                }

                if (resY  > plTarTrl.y)
                {
                    resY -= delta;
                    if (resY < plTarTrl.y) resY = plTarTrl.y;
                }
                else if (resY < plTarTrl.y)
                {
                    resY += delta;
                    if (resY > plTarTrl.y) resY = plTarTrl.y;
                }

                translation.Value = new float3(resX, resY, translation.Value.z);
            }
        }

        protected override JobHandle OnUpdate(JobHandle inputDependencies)
        {
            var playersPos = m_Group_Player.ToComponentDataArray<Pos>(Allocator.TempJob);

            NativeArray<int> actualStepX = new NativeArray<int>(1, Allocator.TempJob);
            NativeArray<int> actualStepY = new NativeArray<int>(1, Allocator.TempJob);

            actualStepX[0] = playersPos[0].x;
            actualStepY[0] = playersPos[0].y;

            var playersTrls = m_Group_Player.ToComponentDataArray<Translation>(Allocator.TempJob);

            var cellsPos = m_Group_Cells.ToComponentDataArray<Pos>(Allocator.TempJob);
            var cellsTrls = m_Group_Cells.ToComponentDataArray<Translation>(Allocator.TempJob);

            var cellsStps = m_Group_Path.ToComponentDataArray<Step>(Allocator.TempJob);

            var jobStep = new StepJob
            {
                Time = Time.realtimeSinceStartup,
            };

            jobStep.playersPos = playersPos;
            jobStep.playersTrls = playersTrls;
            jobStep.cellsTrls = cellsTrls;
            jobStep.cellsPos = cellsPos;
            jobStep.cellsStps = cellsStps;

            jobStep.actualStepX = actualStepX;
            jobStep.actualStepY = actualStepY;

            var jobStepHandle = jobStep.Schedule();
            jobStepHandle.Complete();

            var job = new MoveSpeedJob
            {
                Speed = PlayerSpeed,
                DeltaTime = Time.deltaTime,
                actualStepX = actualStepX,
                actualStepY = actualStepY,
            }.Schedule(this, inputDependencies);

            job.Complete();

            actualStepX.Dispose();
            actualStepY.Dispose();
            playersPos.Dispose();
            playersTrls.Dispose();
            cellsPos.Dispose();
            cellsTrls.Dispose();
            cellsStps.Dispose();

            return job;
        }
    }
}


