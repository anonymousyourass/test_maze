﻿using UnityEngine;
using Unity.Entities;
using Unity.Collections;
using Unity.Rendering;
using Unity.Mathematics;

namespace HeroCraft
{
    public static class Extensions
    {
        public static bool IsSpawnPoint(this Entity e)
        {
            return World.Active.EntityManager.HasComponent<IsSpawnPoint>(e);
        }

        public static bool IsObstacle(this Entity e)
        {
            return World.Active.EntityManager.HasComponent<IsObstacle>(e);
        }

        public static bool IsFlooded(this Entity e)
        {
            return World.Active.EntityManager.HasComponent<Flood>(e);
        }

        public static bool IsGoalPoint(this Entity e)
        {
            return World.Active.EntityManager.HasComponent<IsGoalPoint>(e);
        }

        public static Pos GetPos(this Entity entity)
        {
            return World.Active.EntityManager.GetComponentData<Pos>(entity);
        }

        public static void SetPos(this Entity entity, Pos pos)
        {
            World.Active.EntityManager.SetComponentData<Pos>(entity, pos);
        }

        public static Prob GetProb(this Entity entity)
        {
            return World.Active.EntityManager.GetComponentData<Prob>(entity);
        }

        public static int GetFCost(this Entity cell)
        {
            return cell.GetGCost() + cell.GetHCost();
        }

        public static int GetGCost(this Entity cell)
        {
            return World.Active.EntityManager.GetComponentData<Pos>(cell).gCost;
        }

        public static int GetHCost(this Entity cell)
        {
            return World.Active.EntityManager.GetComponentData<Pos>(cell).hCost;
        }

        public static void SetCost(this Entity cell, int gCost, int hCost)
        {
            var pos = cell.GetPos();

            var newPos = new Pos()
            {
                hCost = hCost,
                gCost = gCost,
                x = pos.x,
                y = pos.y
            };

            World.Active.EntityManager.SetComponentData<Pos>(cell, newPos);
        }

        public static void AddOrSetParent(this Entity cell, Pos own, Pos pos)
        {
            var parent = new Parent() { pos = pos, own = own };
            if (cell.IsParential())
            {
                World.Active.EntityManager.SetComponentData<Parent>(cell, parent);
            }
            else
            {
                World.Active.EntityManager.AddComponentData<Parent>(cell, parent);
            }

        }

        public static void AddOrSetStep(this Entity cell, Pos cur, Pos next)
        {
            var step = new Step() { cur = cur, next = next };
            if (cell.IsStep())
            {
                World.Active.EntityManager.SetComponentData<Step>(cell, step);
            }
            else
            {
                World.Active.EntityManager.AddComponentData<Step>(cell, step);
            }
        }

        public static void AddOrSetFlood(this Entity cell,Flood f)
        {
            if (cell.IsFlooded())
            {
                World.Active.EntityManager.SetComponentData<Flood>(cell, f);
            }
            else
            {
                World.Active.EntityManager.AddComponentData<Flood>(cell, f);
            }
        }


        public static Pos GetParentPos(this Entity cell)
        {
            return World.Active.EntityManager.GetComponentData<Parent>(cell).pos;
        }

        public static bool IsParential(this Entity cell)
        {
            return World.Active.EntityManager.HasComponent<Parent>(cell);
        }

        public static bool IsStep(this Entity cell)
        {
            return World.Active.EntityManager.HasComponent<Step>(cell);
        }

        public static void SetColor(this Entity entity, Color color)
        {
            var renderMesh0 = World.Active.EntityManager.GetSharedComponentData<RenderMesh>(entity);
            renderMesh0.material.SetColor("_EmisColor", color);
            World.Active.EntityManager.SetSharedComponentData<RenderMesh>(entity, renderMesh0);
        }

        //замена FirstOrDefault от Linq не поддерживает типы NativeArray в нем свой Iterator и
        // сам по себе он немного по другому используется - хранит например не на экзмепляры классов а на  struct
        // быстро для него wrapper не сделать ошибка для того чтобы логировать ошибки игровой логики
        public static Entity FirstIsSpawn(this NativeArray<Entity> cells)
        {
            foreach (var cell in cells)
            {
                if (IsSpawnPoint(cell))
                {
                    return cell;
                }
            }
            Debug.LogError("Cant find any spawn point in array " + cells);
            return World.Active.EntityManager.CreateEntity();
        }

        public static Entity FirstWithPos(this NativeArray<Entity> cells, Pos pos)
        {
            foreach (var cell in cells)
            {
                if (cell.GetPos().x == pos.x && cell.GetPos().y == pos.y)
                {
                    return cell;
                }
            }
            Debug.LogError("Cant find any cell in array " + cells);
            return World.Active.EntityManager.CreateEntity();
        }

        public static float2 ToFloat2(this Pos pos)
        {
            return new float2(pos.x, pos.y);
        }

    }
}
