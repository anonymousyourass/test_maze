﻿using Unity.Entities;
using UnityEngine;
using UnityEngine.UI;
using Unity.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Transforms;
using Unity.Physics.Systems;

namespace HeroCraft
{
    public class Generator : MonoBehaviour
    {
        public GameObject prefab;
        public GameObject prefabPlayer;

        public UnityEngine.Material prefabMaterialTile;
        public UnityEngine.Material prefabMaterialPlayerGoal;

        public Button btnGenerate;
        public InputField inputW;
        public InputField inputH;

        private EntityManager eM;
        private int width;
        private int height;
        public static float3 Offset;

        private const float PROB_DISTRIB_ALG_BRAID = 0.5f;
        private const float TILES_Z_DEPTH = 0f;
        public const float ACTOR_Z_DEPTH = -0.1f;
        public const float TILE_MAGN = 1f;
        public const float MOVE_PRECISION = 0.05f;

        private NativeArray<Entity> cells;
        private Entity player;
        private Entity goal;

        private RaycastInput RaycastInput;
        private BuildPhysicsWorld m_BuildPhysicsWorldSystem;

        private List<Entity> path;

        protected void Start()
        {
            Button btn1 = btnGenerate.GetComponent<Button>();
            btn1.onClick.AddListener(onClickGenerate);

            eM = World.Active.EntityManager;
            m_BuildPhysicsWorldSystem = World.Active.GetOrCreateSystem<BuildPhysicsWorld>();
            path = new List<Entity>();
        }


        void OnDisable()
        {
            foreach (var e in cells)
                eM.DestroyEntity(e);
            if (cells.Length > 0)
                cells.Dispose();
            eM.DestroyEntity(player);
            eM.DestroyEntity(goal);
        }

        private void onClickGenerate()
        {
            path.Clear();

            foreach (var e in cells)
                eM.DestroyEntity(e);

            eM.DestroyEntity(player);
            eM.DestroyEntity(goal);

            if (cells.Length > 0)
                cells.Dispose();

            int.TryParse(inputW.text, out width);
            int.TryParse(inputH.text, out height);

            cells = new NativeArray<Entity>(width * height, Allocator.Persistent);

            Offset = new float3(width / 2, height / 2, 0f);

            int idCell = 0;
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    var cell = instantiateFromPrefabNotSharedMaterial(prefab, prefabMaterialTile);
                    decorateTile(cell, Offset, i, j);
                    cells[idCell++] = cell;
                }
            }

            foreach (var e in cells)
            {
                var ePos = e.GetPos();
                if (ePos.x == 0 && ePos.y == 0)
                {
                    eM.AddComponentData<IsSpawnPoint>(e, new IsSpawnPoint());
                    player = instantiateFromPrefabNotSharedMaterial(prefabPlayer, prefabMaterialPlayerGoal);
                    decorateActor(player, Offset, ePos);
                    eM.AddComponentData<IsPlayer>(player, new IsPlayer());
                }
                if (ePos.x == width - 1 && ePos.y == height - 1)
                {
                    eM.AddComponentData<IsGoalPoint>(e, new IsGoalPoint());
                    goal = instantiateFromPrefabNotSharedMaterial(prefabPlayer, prefabMaterialPlayerGoal);
                    decorateActor(goal, Offset, ePos);
                }
            }

            braidGen(cells);

            foreach (var entity in cells)
            {
                if (entity.IsObstacle())
                {
                    entity.SetColor(Color.red);
                }
                else
                {
                    entity.SetColor(Color.white);
                }
            }
        }

        private void decorateTile(Entity tile, float3 offset, int col, int row)
        {
            var pos = new Pos() { x = col, y = row};
            eM.AddComponentData<Pos>(tile, pos);
            var transl = new Translation() { Value = new float3(col * TILE_MAGN - offset.x, row * TILE_MAGN - offset.y, TILES_Z_DEPTH) };
            eM.SetComponentData<Translation>(tile, transl);
            var prob = new Prob();
            var rand01 = UnityEngine.Random.Range(0f, 1f);
            prob.Value = rand01 > PROB_DISTRIB_ALG_BRAID ? 2 : 3;
            eM.AddComponentData<Prob>(tile, prob);
        }

        private void decorateActor(Entity actor, float3 offset, Pos spawnPos)
        {
            var pos = new Pos() { x = spawnPos.x, y = spawnPos.y};
            eM.AddComponentData<Pos>(actor, pos);
            var playerTranlsation = new Translation();
            playerTranlsation.Value = new float3(spawnPos.x * TILE_MAGN - offset.x, spawnPos.y * TILE_MAGN - offset.y, ACTOR_Z_DEPTH);
            eM.SetComponentData<Translation>(actor, playerTranlsation);
        }

        //создаем entity в ужасных 6 инструкций к Unity API вместо 1 изза того что используем 
        //SharedComponentData Color для перекрашивания отдельных RenderMesh.Material
        private Entity instantiateFromPrefabNotSharedMaterial(GameObject originalPrefab, UnityEngine.Material instanciatedMaterial)
        {
            GameObject prefabSource = Instantiate(originalPrefab, Vector3.one * 1000f, Quaternion.identity);
            prefabSource.GetComponent<Renderer>().material = Instantiate(instanciatedMaterial);
            prefabSource.GetComponent<Renderer>().material.SetColor("_EmisColor", Color.black); //todo check -
            Entity sourceEntity = GameObjectConversionUtility.ConvertGameObjectHierarchy(prefabSource, World.Active);
            Destroy(prefabSource);
            return eM.Instantiate(sourceEntity);
        }

        void Update()
        {
#if UNITY_EDITOR
            if (Input.GetMouseButton(0))
#else
		if(Input.touchCount == 1)
#endif
            {
#if UNITY_EDITOR
                UnityEngine.Ray ray = Camera.main.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
#else
			UnityEngine.Ray ray = Camera.main.ScreenPointToRay(new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, 0));
#endif
                float3 origin = ray.origin;
                float3 direction = ray.direction;

                RaycastInput = new RaycastInput
                {
                    Start = origin,
                    End = origin + direction * math.abs(Camera.main.transform.position.z) * 2f,
                    Filter = CollisionFilter.Default
                };

                if (m_BuildPhysicsWorldSystem.PhysicsWorld.CastRay(RaycastInput, out Unity.Physics.RaycastHit hit))
                {
                    if (hit.RigidBodyIndex == -1)
                        return;

                    Entity cellTarget = m_BuildPhysicsWorldSystem.PhysicsWorld.Bodies[hit.RigidBodyIndex].Entity;
                    var playerCell = cells.FirstWithPos(player.GetPos());
                    if (cellTarget.IsObstacle() == false && playerCell.IsGoalPoint() == false)
                    {
                        var newPath = cells.FindPathAstar(playerCell, cellTarget);
                        if (newPath.Count > 0)
                        {
                            setPathColor(Color.white);
                            path = newPath;
                            setPathColor(Color.blue);
                        }
                    }
                }
            }
        }

        private void setPathColor(Color color)
        {
            foreach (var pathCell in path)
            {
                pathCell.SetColor(color);
            }
        }

        private void braidGen(NativeArray<Entity> cells)
        {
            foreach (var entity in cells)
            {
                var neighbours = cells.GetNeighbours(entity);

                foreach (var neighbour in neighbours)
                {
                    neighbour.SetColor(Color.white);
                    if (entity.GetProb().Value >= cells.GetNeighbours(entity).Count)
                    {
                        break;
                    }
                    if (neighbour.GetProb().Value >= cells.GetNeighbours(neighbour).Count)
                    {
                        continue;
                    }
                    eM.AddComponentData<IsObstacle>(neighbour, new IsObstacle());
                    if (cells.FloodToGoal(cells.FirstIsSpawn()) == false)
                    {
						foreach(var c in cells)
						{
							eM.RemoveComponent<Flood>(c);
						}
                        eM.RemoveComponent<IsObstacle>(neighbour);
                    }
                }
            }
        }

        private bool inBounds(Pos pos)
        {
            if (pos.x >= 0 && pos.y >= 0 && pos.x < width && pos.y < height)
            {
                return true;
            }
            return false;
        }

    }
}



