﻿using System.Collections.Generic;
using Unity.Entities;
using Unity.Collections;
using Unity.Mathematics;

namespace HeroCraft
{
    public static class Algorithms
    {
        public static bool FloodToGoal(this NativeArray<Entity> cells, Entity cell)
        {
            if (cell.IsObstacle() || cell.IsFlooded())
                return false;

            World.Active.EntityManager.AddComponentData<Flood>(cell, new Flood());

            if (cell.IsGoalPoint())
                return true;

            var neighbours = cells.GetNeighbours(cell);
            
            foreach (var neighbour in neighbours)
            {
                if (cells.FloodToGoal(neighbour))
                    return true;
            }

            return false;
        }

        public static List<Entity> FindPathAstar(this NativeArray<Entity> cells, Entity start, Entity target)
        {

            List<Entity> foundPath = new List<Entity>();
            List<Entity> openSet = new List<Entity>();
            HashSet<Entity> closedSet = new HashSet<Entity>();

            openSet.Add(start);

            while (openSet.Count > 0)
            {
                Entity currentNode = openSet[0];

                for (int i = 0; i < openSet.Count; i++)
                {
                    if (openSet[i].GetFCost() < currentNode.GetFCost() ||
                        (openSet[i].GetFCost() == currentNode.GetFCost() &&
                        openSet[i].GetHCost() < currentNode.GetHCost()))
                    {
                        if (currentNode != openSet[i])
                        {
                            currentNode = openSet[i];
                        }
                    }
                }

                openSet.Remove(currentNode);
                closedSet.Add(currentNode);

                if (currentNode == target || currentNode.IsGoalPoint())
                {
                    foundPath = cells.RetracePath(start, currentNode);
                    return foundPath;
                }

                foreach (Entity neighbour in cells.GetNeighbours(currentNode))
                {
                    if (closedSet.Contains(neighbour) == false)
                    {
                        int newMovementCostToNeighbour = currentNode.GetGCost() + GetDistance(currentNode, neighbour);
                        if (newMovementCostToNeighbour < neighbour.GetGCost() || openSet.Contains(neighbour) == false)
                        {
                            var neighbourToTarget = GetDistance(neighbour, target);
                            neighbour.SetCost(newMovementCostToNeighbour, neighbourToTarget);
                            neighbour.AddOrSetParent(neighbour.GetPos(), currentNode.GetPos());

                            if (openSet.Contains(neighbour) == false)
                            {
                                openSet.Add(neighbour);
                            }
                        }
                    }
                }
            }
            return foundPath;
        }

        public static List<Entity> RetracePath(this NativeArray<Entity> cells, Entity startNode, Entity endNode)
        {
            foreach (var c in cells)
            {
                World.Active.EntityManager.RemoveComponent<Step>(c);
            }

            List<Entity> path = new List<Entity>();
            Entity currentNode = endNode;
            path.Add(currentNode);
            do
            {
                var parentPos = new Pos() { x = currentNode.GetParentPos().x, y = currentNode.GetParentPos().y };
                currentNode.AddOrSetStep(currentNode.GetPos(), cells.FirstWithPos(parentPos).GetPos());
                currentNode = cells.FirstWithPos(parentPos);
                path.Add(currentNode);
            }
            while (currentNode != startNode);
            path.Reverse();

            foreach (var c in cells)
            {
                World.Active.EntityManager.RemoveComponent<Parent>(c);
            }

            return path;
        }

        //установить diagonal = true и генерация и поиск пути будет с возможностью диагональных переходов
        public static List<Entity> GetNeighbours(this NativeArray<Entity> cells, Entity cell, bool diagonal = false)
        {
            List<Entity> neighbours = new List<Entity>();
            var posCell = cell.GetPos();
            foreach (var c in cells)
            {
                if (cell != c && c.IsObstacle() == false)
                {
                    var posNeigbour = c.GetPos();
                    if (posNeigbour.x == posCell.x + 1 && posNeigbour.y == posCell.y ||
                        posNeigbour.x == posCell.x - 1 && posNeigbour.y == posCell.y ||
                        posNeigbour.x == posCell.x && posNeigbour.y + 1 == posCell.y ||
                        posNeigbour.x == posCell.x && posNeigbour.y - 1 == posCell.y
                        )
                    {
                        neighbours.Add(c);
                    }
                    if (diagonal)
                    {
                        if (posNeigbour.x == posCell.x + 1 && posNeigbour.y + 1 == posCell.y ||
                        posNeigbour.x == posCell.x - 1 && posNeigbour.y - 1 == posCell.y ||
                        posNeigbour.x == posCell.x + 1 && posNeigbour.y - 1 == posCell.y ||
                        posNeigbour.x == posCell.x - 1 && posNeigbour.y + 1 == posCell.y
                        )
                        {
                            neighbours.Add(c);
                        }
                    }
                }
            }
            return neighbours;
        }

        public static int GetDistance(Entity cell1, Entity cell2)
        {
            var lenF = math.length(cell2.GetPos().ToFloat2() - cell1.GetPos().ToFloat2());
            int len = (int)math.round(lenF * 10f);
            return len;
        }
    }

}

